# EjercicioRecursiva 09/05/2022
## Que necesito si estoy usando Windows
Si usan Windows, será necesario descargar MSYS2 para poder compilar y correr el ejercicio de C++.
Si no está instalado, seguir los pasos del siguiente link: https://www.msys2.org/

## Como ejecutar el ejercicio desde cualquier sistema operativo
Clonar o descargar el repositorio
Ir a la carpeta donde contiene los archivos y abrir la consola allí
Escribir el siguiente comando:
g++ -DCANT_SOCIOS .\main.cpp -o cant_socios && g++ -DPROM_EDAD_RACING .\main.cpp -o prom_edad_racing && g++ -DCASADOS_UNIV .\main.cpp -o casados_universitarios && g++ -DNOMBRES_RIVER .\main.cpp -o nombres_river &&  g++ -DEDADES_EQUIPOS .\main.cpp -o edades_equipos && g++ -DCANT_SOCIOS -DPROM_EDAD_RACING -DCASADOS_UNIV -DNOMBRES_RIVER -DEDADES_EQUIPOS .\main.cpp -o ej_completo

De esta manera, se crean 6 archivos ejecutables en la carpeta actual que permiten obtener los resultados de cada ejercicio por separado o todos juntos:
- Para solo obtener la cantidad de socios (ejercicio 1), ejecutar: "cant_socios *ruta del archivo de socios*"
- Para solo obtener el promedio de edad de los socios de Racing (ejercicio 2): "prom_edad_racing *ruta del archivo de socios*"
- Para solo obtener las 100 personas casadas y universitarias mas jovenes (ejercicio 3): "casados_universitarios *ruta del archivo de socios*"
- Para solo obtener los 5 nombres mas comunes de los hinchas de River (ejercicio 4): "nombres_river *ruta del archivo de socios*"
- Para solo obtener el promedio de edad, la minima y la maxima de cada equipo (ejercicio 5): "edades_equipos *ruta del archivo de socios*"
- Para obtener todos los resultados anteriores juntos (ejercicio 5): "ej_completo *ruta del archivo de socios*"

Si incluyen el archivo de socios en la misma carpeta donde tengan los ejecutables, alcanzaría con escribir "ej_completo socios.csv".

Ante cualquier consulta o sugerencia, no duden en escribirme a bpaez2@gmail.com

Maria Belen Paez
