#include <string>
#include <cstring> //Para poder usar el strcasecmp
#include <vector>
#include <iostream> //Para usar cin y cout
#include <algorithm> //Para sort de vectores
#include <fstream>
#include <sstream>
#include <locale.h> //Para respetar formatos
using namespace std;

//Declaro el vector que me servira para guardar los socios del archivo
struct socio_s{
    const string getNombre(){
        return nombre;
    }
    const int getEdad(){
        return edad;
    }
    const string getEquipo(){
        return equipo;
    }
    const string getEstadoCivil(){
        return estadoCivil;
    }
    const string getEstudios(){
        return estudios;
    }

    string nombre, equipo, estadoCivil, estudios;
    int edad;
};
int cantSocios;
vector<socio_s> casados_univ;
//Creo un struct para poder referirme a un nombre y a su cantidad de repeticiones
struct nombre_s{ 
    string nombre; 
    int repeticiones;
    string getNombre(){
        return nombre;
    }
    int getRepet(){
        return repeticiones;
    }
    void incRepet(){
        repeticiones += 1;
    }
};
vector<nombre_s> hinchasRiver;
struct equipo_s{
    string nombre;
    int sumaEdad;
    int cantidadSocios;
    int menorEdad;
    int mayorEdad;

    void agregarSocio(int edad){
        sumaEdad += edad;
        cantidadSocios += 1;
    }
    void compararMenorEdad(int edad){
        int actual = menorEdad;
        if(actual > edad){
            menorEdad = edad;
        }
    }
    void compararMayorEdad(int edad){
        int actual = mayorEdad;
        if(actual < edad){
            mayorEdad = edad;
        }
    }

    //Retornar datos
    string getNombre(){
        return this->nombre;
    }
    int getsumaEdad(){
        return this->sumaEdad;
    }
    int getMenorEdad(){
        return this->menorEdad;
    }
    int getMayorEdad(){
        return this->mayorEdad;
    }
    int getCantidadSocios(){
        //return this->edades.size();
        return this->cantidadSocios;
    }
};
vector<equipo_s> equipos;
int sumaEdadesRacing;
int cantidadSociosRacing;

//EJERCICIO 1
//Funcion que agrega un socio al vector
void agregoSocio(socio_s *socio, string nombre, string edad_s, string equipo, string estadoCivil, string estudios){
    socio->nombre = nombre;
    socio->edad = stoi(edad_s);
    socio->equipo = equipo;
    socio->estadoCivil = estadoCivil;
    socio->estudios = estudios;
}
//Funcion que imprime la cantidad de socios obtenidos
void devuelvoCantidadSocios(){
    if(cantSocios > 1) { cout << "Hay " << cantSocios << " personas registradas" << endl;}
    else if(cantSocios == 1) { cout << "Hay " << cantSocios << " persona registrada" << endl;}
    else { cout << "No hay personas registradas" << endl;}
}

//EJERCICIO 2
void sumoEdadesSociosRacing(int i, socio_s *socio){
    if(strcasecmp("Racing", socio->getEquipo().c_str()) == 0){
        //Si hay un socio, sumo su edad
        sumaEdadesRacing += socio->getEdad();
        //Incremento la cantidad de socios
        cantidadSociosRacing += 1;
    }
}
void promedioEdadSociosRacing(){
    if(cantidadSociosRacing > 0){
        int promedio = sumaEdadesRacing/cantidadSociosRacing;
        //Si hay mas de un socio de racing, devuelvo el promedio
        cout << "El promedio de edad de los hinchas de Racing es: " << promedio << endl;
    } else {
        //Devuelvo que no hay hinchas de racing
        cout << "No hay hinchas de Racing" << endl;
    }

}

//EJERCICIO 3
//Funcion auxiliar para ordenar los socios segun su edad de menor a mayor
bool porEdadMenor(socio_s i, socio_s j){
    return (i.getEdad() == j.getEdad() || i.getEdad() < j.getEdad());
}
void agregoUsuariosCasadosUniversitarios(int i, socio_s *socio){
    if(strcasecmp("Casado", socio->getEstadoCivil().c_str()) == 0 &&
       strcasecmp("Universitario", socio->getEstudios().c_str()) == 0){
            casados_univ.push_back(*socio);
    }
}
void devuelvoCasadosUniversitarios(){
    //Ordeno los socios casados universitarios por edad de menor a mayor
    stable_sort(casados_univ.begin(), casados_univ.end(), porEdadMenor);

    if(casados_univ.size() == 0){
        //Si no hay personas que cumplan, lo indico
        cout << "No hay personas casadas y con estudios universitarios" << endl;
    } else  if(casados_univ.size() < 100){
        //Si no hay 100 personas casadas y universitarias, no indico dicha cantidad ni que son los mas jovenes
        cout << "Las personas casadas y con estudios universitarios son: " << endl;
    } else {
        //Si son 100 o mas, indico que devuelvo las 100 primeras
        cout << "Las 100 personas casadas y con estudios universitarios mas jovenes son: " << endl;
    }
    for(int i = 0; i < casados_univ.size() && i < 100; i++){
        cout << "Nombre: " << casados_univ[i].getNombre();
        cout << ", edad: " << casados_univ[i].getEdad();
        cout << ", equipo: " << casados_univ[i].getEquipo() << endl; 
    }
}

//EJERCICIO 4
//Funcion auxiliar para ordenar los nombres segun sus repeticiones de menor a mayor
bool porRepeticiones(nombre_s i, nombre_s j){
    return (i.getRepet() == j.getRepet() || i.getRepet() < j.getRepet());
}
void agregoHinchasRiver(int i, socio_s *socio){
    if(strcasecmp("River", socio->getEquipo().c_str()) == 0){
        bool estabaElNombre = false;

        //Lo busco en el listado de hinchas de river
        for (int j = 0; j < hinchasRiver.size(); j++){
            if(strcasecmp(hinchasRiver[j].getNombre().c_str(), socio->getNombre().c_str()) == 0){
                //Si esta, aumento el numero de repeticiones y asigno que estaba
                estabaElNombre = true;
                hinchasRiver[j].incRepet();
                break;
            }
        }

        //Si no estaba en el listado, lo agrego
        if(!estabaElNombre){
            nombre_s nombre;
            nombre.nombre = socio->getNombre();
            nombre.repeticiones = 1;
            hinchasRiver.push_back(nombre);
        }
    }
}
void cincoNombresMasComunesHinchasRiver(){
    //Ordeno a los nombres por la cantidad de repeticiones de menor a mayor
    stable_sort(hinchasRiver.begin(), hinchasRiver.end(), porRepeticiones);

    if(hinchasRiver.size() == 0){
        //Si no hay hinchas de river
        cout << "No hay hinchas de River";
    } else if(hinchasRiver.size() < 5){
        //Si son menos de 5 nombres, no indico la cantidad
        cout << "Los nombres mas comunes de los hinchas de River son: ";
    } else {
        //Devuelvo los 5 nombres mas comunes
        cout << "Los 5 nombres mas comunes de los hinchas de River son: ";
    }
    for(int i = 1; i < 6 && i-1 < hinchasRiver.size(); i++){
        //Obtengo los nombres con mayor cantidad de repeticiones (desde el final del vector)
        if(i>1){ cout << ", "; }
        cout << hinchasRiver[hinchasRiver.size()-i].getNombre();
    }
    cout << endl;

}

//EJERCICIO 5
//Funcion auxiliar para ordenar los equipos segun la cantidad de socios de menor a mayor
bool porCantSocios(equipo_s i, equipo_s j){
    return (i.cantidadSocios == j.cantidadSocios || i.cantidadSocios < j.cantidadSocios);
}
void agregoEquiposAListado(int s, socio_s *socio){
    bool estabaElEquipo = false;

    //Lo busco en el listado de equipos
    for (int i = 0; i<equipos.size(); i++){
        if(strcasecmp(equipos[i].nombre.c_str(), socio->getEquipo().c_str()) == 0){
            //Si esta, agrego el socio a dicho equipo y asigno que estaba
            estabaElEquipo = true;
            equipos[i].agregarSocio(socio->getEdad());
            equipos[i].compararMenorEdad(socio->getEdad());
            equipos[i].compararMayorEdad(socio->getEdad());
            break;
        }
    }

    //Si no estaba en el listado, lo agrego
    if(!estabaElEquipo){
        equipo_s equipo;
        equipo.nombre = socio->getEquipo();
        equipo.cantidadSocios = 1;
        equipo.sumaEdad = socio->getEdad();
        equipo.mayorEdad = socio->getEdad();
        equipo.menorEdad = socio->getEdad();
        equipos.push_back(equipo);
    }
}
void datosDeEdadesPorEquipos(){
    //Ordeno los equipos por cantidad de socios de menor a mayor
    stable_sort(equipos.begin(), equipos.end(), porCantSocios);

    if(equipos.size() > 0){
        //Imprimo los promedios de edad, las edades minimas y maximas de cada equipo
        cout << "Datos de edades por equipos ordenados segun cantidad de socios descendiente: " << endl;
    }else{
        cout << "No hay equipos registrados" << endl;
    }
    for(int i = equipos.size()-1; i >= 0; i--){
        //Obtengo los equipos con mayor cantidad de socios (desde el final del vector)
        cout << "Equipo: " << equipos[i].nombre;
        cout << ", promedio de edad: " << equipos[i].sumaEdad/equipos[i].cantidadSocios;
        cout << ", edad minima: " << equipos[i].menorEdad;
        cout << ", edad maxima: " << equipos[i].mayorEdad << endl;
    }

}

int main(int argc, char *argv[]){
    //Para poder imprimir con caracteres especiales (ej: acentos)
    setlocale(LC_CTYPE, "spanish");

    ifstream file;
    file.open(argv[1], ios::in);

    //Inicializo variables y vector de socios
    string nombre, edad_s, equipo, estadoCivil, estudios;
    socio_s socio;
    cantSocios = 0;
    sumaEdadesRacing = 0;
    cantidadSociosRacing = 0;
    casados_univ = {};
    hinchasRiver = {};
    equipos = {};
    
    while(true){
        //Leo cada elemento de la linea y lo guardo en una variable en particular
        getline(file, nombre, ';');
        if(nombre.size()==0){break;}
        getline(file, edad_s, ';');
        getline(file, equipo, ';');
        getline(file, estadoCivil, ';');
        getline(file, estudios, '\n');

        //Guardo los datos del socio en el vector socios
        agregoSocio(&socio, nombre, edad_s, equipo, estadoCivil, estudios);
    
        //Busco en los registrados los socios de racing
        #ifdef PROM_EDAD_RACING
        sumoEdadesSociosRacing(cantSocios, &socio);
        #endif

        //Busco en los registrados los que cumplan que son casados y universitarios#endif
        #ifdef CASADOS_UNIV
        agregoUsuariosCasadosUniversitarios(cantSocios, &socio);
        #endif

        //Busco en los registrados los socios de river
        #ifdef NOMBRES_RIVER
        agregoHinchasRiver(cantSocios, &socio);
        #endif

        //Categorizo a los registrados por equipos
        #ifdef EDADES_EQUIPOS
        agregoEquiposAListado(cantSocios, &socio);
        #endif

        cantSocios += 1;
    }
    file.close();

    //Llamo a las funciones de los ejercicios
    #ifdef CANT_SOCIOS 
    devuelvoCantidadSocios();
    #endif
    #ifdef PROM_EDAD_RACING
    promedioEdadSociosRacing();
    #endif
    #ifdef CASADOS_UNIV
    devuelvoCasadosUniversitarios();
    #endif
    #ifdef NOMBRES_RIVER
    cincoNombresMasComunesHinchasRiver();
    #endif
    #ifdef EDADES_EQUIPOS
    datosDeEdadesPorEquipos();
    #endif

    return 0;    
}
